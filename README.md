# My personal overlay for Gentoo

This is my own personal overlay with packages that I use.

## Adding the overlay

First, install git if not already:
```
# emerge --ask --verbose dev-vcs/git
```
Then, create a file called dietnusse.conf in /etc/portage/repos.conf as such:
```
[dietnusse]
location = /var/db/repos/dietnusse
sync-type = git
sync-uri = https://gitlab.com/federatives/dietnusse.git
clone-depth = 1
sync-depth = 1
auto-sync = yes
```
Now, sync the overlay itself;
```
# emaint sync -r dietnusse
```
You should be all done now!
